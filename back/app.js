const express = require('express')
const app = express()
const port = 3210
const cors = require('cors')
const cookieParser = require('cookie-parser')

app.use(cookieParser())

app.use(cors({
    origin: ['http://localhost:3001'],
    credentials: true,
}))

app.get('/users', (req, res) => {
    res
        .cookie('accessToken', 'token', {
            httpOnly: true,
            secure: true,
            sameSite: 'none',
            domain: '',
        })
    res
        .status(200)
        .send([
            {
                name: 'Lorenzo',
                lastName: 'Muñoz',
            },
            {
                name: 'Joaquin',
                lastName: 'Muñoz',
            }
        ]);
})

app.get('/admin/users', (req, res) => {
    console.log('Cookies: ', req.cookies)
    res
        .status(200)
        .send('OK');
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})