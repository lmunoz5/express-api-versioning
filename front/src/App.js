import React, { useState, useEffect } from 'react';
import logo from './logo.svg';
import axios from 'axios';
import './App.css';

function App() {
  const [users, setUsers] = useState([]);



  useEffect(() => {
    const getUsers = async () => {
      const response = await axios.get(
        'http://localhost:3210/users',
        {
          withCredentials: true,
        }
      )
      if (response.status === 200) {
        setUsers(response.data);
      }
    }
    getUsers();
  }, []);

  return (
    <ul>
      {users.map((user, index) => 
        <li key={index}>{user.name} {user.lastName}</li>
      )}
    </ul>
  );
}

export default App;
